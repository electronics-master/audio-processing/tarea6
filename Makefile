PROJECT = tarea6
LANG = es
SHELL := /bin/bash
SRC = $(wildcard *.tex)
TIKZ_DIR = figures
TIKZ_SRC := $(wildcard $(TIKZ_DIR)/*.tikz)
TIKZ_IMGS := $(patsubst %.tikz, %.pdf, $(TIKZ_SRC))

.PHONY: all clean check

all: $(PROJECT).pdf

tikz2pdf: $(TIKZ_IMGS)

# MAIN LATEXMK RULE
$(PROJECT).pdf: $(SRC) $(TIKZ_IMGS)
	latexmk -f -bibtex -use-make -pdf -pdflatex="pdflatex -synctex=1 -shell-escape" $(PROJECT).tex

%.pdf: %.tikz
	latexmk -pdf $< -outdir=$(TIKZ_DIR) 

check: $(SRC)
	aspell --lang=$(LANG) -t -c $<

indent: $(SRC)
	latexindent -s -m --overwrite $^

clean:
	latexmk -C




